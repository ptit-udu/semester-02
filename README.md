# PTIT-UDU Project - Semester-02

This project contains teaching materials for subjects in semester 02 of PTIT-UDU program. Each subject includes:
- Slides, Presentations
- Book, Documentations
- Samples
- Experiments
- Others

